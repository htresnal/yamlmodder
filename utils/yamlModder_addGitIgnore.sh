#!/bin/sh


if ! command -v git &> /dev/null
then
    echo "Please install Git to use this command"
    exit 1
fi

## Place into the root of your game folder and execute.
git add .gitignore
git commit -m "Added gitignore"
