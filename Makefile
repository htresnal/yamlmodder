CC=c++
CFLAGS=-std=c++17 -o2
LDFLAGS=-lyaml-cpp
EXECUTABLE=yamlmodder

all: $(EXECUTABLE)

$(EXECUTABLE):
	$(CC) main.cpp -o $(EXECUTABLE) $(LDFLAGS)

$(EXECUTABLE).o:
	$(CC) $(CFLAGS) main.cpp

clean:
	rm ./$(EXECUTABLE)

install:
	sudo cp ./$(EXECUTABLE) /usr/local/bin/$(EXECUTABLE)

uninstall:
	sudo rm -f /usr/local/bin/$(EXECUTABLE)
