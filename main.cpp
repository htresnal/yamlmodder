#include <iostream>
#include <string>
#include <filesystem>
#include <cctype>
#include <vector>
#include <algorithm>
#include <yaml-cpp/emittermanip.h>
#include <yaml-cpp/node/type.h>
#include <yaml-cpp/yaml.h>
#include "ModEntry.h"


using namespace std;
namespace fs = filesystem; 


string str_toupper(string s)
{
    transform(s.begin(), s.end(), s.begin(),
                [](unsigned char c){ return toupper(c); }
    );
    return s;
}

void printHelp()
{
    cout << "Usage: ./yamlmodder [-vsS] <yaml_path> <mod_path> <install_path>\n";
    cout << "[-v, --verbose] -- Verbose mode.\n";
    cout << "[-s, --sort] -- Sort mods alphabetically.\n";
    cout << "[-S, --ignore-case-sort] -- Sort mods alphabetically, ignoring cases.";
}

struct G_Settings
{
    bool verbose = false;
    bool doSort = false;
    bool doUppercaseSort = false;
    string yamlPath;
    string modsPath;
    string installPath;

    int parseSettings(int argc, char **argv)
    {
        vector<string> args;
        for(size_t i = 0; i<argc; i++)
        {
            args.push_back(argv[i]);
        }

        int path_arg_shift = 0;

        if (args.size() < 4){
            printHelp();

            return -1;
        } else if (find(args.begin(), args.end(), "-h") != args.end()
                || find(args.begin(), args.end(), "--help") != args.end()
        )
        {
            printHelp();

            return 1;
        }

        if (find(args.begin(), args.end(), "-v") != args.end()
         || find(args.begin(), args.end(), "--verbose") != args.end())
        {
            this->verbose = true;

            path_arg_shift++;
        }

        if (find(args.begin(), args.end(), "-s") != args.end()
         || find(args.begin(), args.end(), "--sort") != args.end())
        {
            this->doSort = true;

            path_arg_shift++;
        }

        if (find(args.begin(), args.end(), "-S") != args.end()
         || find(args.begin(), args.end(), "--ignore-case-sort") != args.end())
        {
            this->doSort = true;
            this->doUppercaseSort = true;

            path_arg_shift++;
        }

        if (find(args.begin(), args.end(), "-vs") != args.end()
         || find(args.begin(), args.end(), "-sv") != args.end()
        )
        {
            this->verbose = true;
            this->doSort = true;

            path_arg_shift++;
        }

        if (find(args.begin(), args.end(), "-vS") != args.end()
         || find(args.begin(), args.end(), "-Sv") != args.end()
        )
        {
            this->verbose = true;
            this->doSort = true;
            this->doUppercaseSort = true;

            path_arg_shift++;
        }

        // Map the arguments
        this->yamlPath = args.at(1+path_arg_shift);
        this->modsPath = args.at(2+path_arg_shift);
        this->installPath = args.at(3+path_arg_shift);

        return 0;
    }
} g_settings;

struct G_Modder{
    YAML::Node modsNode;

    int buildFromYaml(const fs::path &yamlPath)
    {
        modsNode = YAML::LoadFile(yamlPath);

        return 0;
    }

    void parseMod(ModEntry &mod, YAML::Node &modInnerNode)
    {
        for (size_t j=0; j<modInnerNode.size(); j++)
        {
            if (modInnerNode[j].Type() != YAML::NodeType::Map) continue;

            for(YAML::const_iterator it=modInnerNode[j].begin();it!=modInnerNode[j].end();++it)
            {
                string entryName = it->first.as<string>();
                if (entryName == "enabled")
                {
                    const YAML::Node &enabledNodeArray = it->second;

                    if (enabledNodeArray.size() < 1) continue;

                    for (size_t k=0; k<enabledNodeArray.size(); k++)
                    {
                        if (enabledNodeArray[k].Type() == YAML::NodeType::Scalar
                         && enabledNodeArray[k].as<string>() == "true")
                        {
                            mod.enabled = true;
                        }
                    }
                } else if (entryName == "files")
                {
                    mod.files = &(it->second);
                }
            }
        }
    }

    int applyMods()
    {
        vector<ModEntry> modList;

        // Iterating over the inner array of mod folders
        for (size_t i=0; i<this->modsNode.size(); i++)
        {
            const YAML::Node &modEntryArrayNode = this->modsNode[i];

            for (YAML::const_iterator it=modEntryArrayNode.begin(); it!=modEntryArrayNode.end(); ++it)
            {
                ModEntry mod;
                mod.name = it->first.as<string>();

                YAML::Node arrayNode = it->second;

                parseMod(mod, arrayNode);

                modList.push_back(mod);
            }
        }

        if (modList.size() == 0)
        {
            cout << "No active mods found. Bailing...\n";

            return 1;
        }

        const fs::path installPath{g_settings.installPath};
        const fs::path modsPath{g_settings.modsPath};

        vector<fs::path> copyPaths;

        if (g_settings.doSort)
        {
            if (g_settings.doUppercaseSort)
            {
                // Sort mods alphabetically with uppercasing
                sort(modList.begin(), modList.end(), [](const ModEntry &mea, const ModEntry &meb) {
                    return str_toupper(mea.name).compare(str_toupper(meb.name)) < 0;
                });
            } else
            {
                // Sort mods alphabetically
                sort(modList.begin(), modList.end(), [](const ModEntry &mea, const ModEntry &meb) {
                    return mea.name.compare(meb.name) < 0;
                });
            }
        }

        cout << "Mods found: "<<modList.size()<<'\n';
        cout << "\nMod list:\n";

        // Iterating over all installed mods and pulling copy paths
        for (const ModEntry &mod : modList)
        {
            if (!mod.enabled || !mod.files) continue;

            copyPaths.push_back(""s + modsPath.string() + mod.name + "/files");
            cout << mod.name << '\n';
        }

        // Copying and pasting
        cout << "\nInstalling into: " << installPath.string() << '\n';
        if (!fs::exists(installPath)) fs::create_directories(installPath);

        size_t installedModCount = 0;

        for (const fs::path &modPath : copyPaths)
        {
            if (g_settings.verbose){
                cout << "Copying mod: " << modPath.string() << '\n';
            }

            const auto copyOptions = fs::copy_options::overwrite_existing | fs::copy_options::recursive;

            fs::copy(modPath,
                    installPath,
                    copyOptions);

            installedModCount++;
        }

        cout << "\nMods installed: " << installedModCount << '\n';

        return 0;
    }
} g_modder;

int main(int argc, char **argv)
{
    int result = g_settings.parseSettings(argc, argv);
    if (result != 0) return result;

    if (!fs::exists(g_settings.modsPath) || !fs::is_directory(g_settings.modsPath))
    {
        cout << "Invalid mods path: " << g_settings.modsPath << ". No such directory.";
        return -2;
    }

    if(!fs::exists(g_settings.yamlPath) || !fs::is_regular_file(g_settings.yamlPath))
    {
        cout << "Invalid YAML path: " << g_settings.yamlPath << ". No such file.\n";

        return -3;
    }

    const fs::path yamlPath{g_settings.yamlPath};
    const fs::path modsPath{g_settings.modsPath};

    result = g_modder.buildFromYaml(yamlPath);
    if (result < 0)
    {
        cout << "Failed to build modder from YAML. Check if the YAML fits the template\n";
        return -4;
    }

    result = g_modder.applyMods();
    if (result < 0)
    {
        cout << "Failed to apply mods\n";
        return -5;
    }

    cout << "Done.\n";

    return 0;
}
