# YamlModder
https://gitlab.com/htresnal/yamlmodder

_version 0.1a_ <br/>

**installMods.sh** - Uses YamlStruct to create a yaml preset. Then uses YamlModder to copy mods over to the game folder, according to the yaml preset structure. <br/>
**cleanMods.sh** - Removes new, and restores changed game files. Requires **gameCleanInit.sh** to be executed once, at the game folder. <br/>
**gameCleanInit.sh** - Initializes the game folder for modding.

## Requirements
- C++17 compiler
- make
- jbeder's yaml-cpp https://github.com/jbeder/yaml-cpp
- htresnal's YamlStruct https://gitlab.com/htresnal/yamlstruct
- git

## Prerequisites
```bash
# Debian:

sudo apt-get install build-essential libyaml-cpp-dev git

# Arch:

sudo pacman -S base-devel yaml-cpp git

# Manjaro:

sudo pamac install base-devel yaml-cpp git
```

## Installation
```bash
# Build and install

make
make install

# Remove

make uninstall
```

***
## Usage
yamlmodder [-vsS] <yaml_path> <mod_path> <install_path> <br/>
[-v, --verbose] -- Verbose mode. <br/>
[-s, --sort] -- Sort mods alphabetically. <br/>
[-S, --ignore-case-sort] -- Sort mods alphabetically, ignoring cases. <br/>

<yaml_path> - Path to the YAML preset file.<br/>
<mod_path> - Mods folder. Each mod must be in the mod template format. <br/>
<install_path> - Game root directory. <br/>

### What is a mod template?
```yaml
moddingFolder: [
    mods: {
        mod_template:
            enabled: [true],
            files: []
    },
    cleanMods.sh,
    installMods.sh
]
```

**Mod template** is a folder that contains 2 other folders: **enabled** and **files** <br/>
**enabled** folder can contain a file named **true** <br/>
**files** folder contains mod files<br/>

## Quick start
**Step 1: Initialize your game as a Git project** <br/>
Copy file _gameCleanInit.sh_ and execute it at the game's root directory

**Step 2: Copy over the ready modding folder** <br/>
1. Copy and rename _modsForGameX_. <br/>
2. Edit INSTALL_DIR at _cleanMods.sh_ and _installMods.sh_ with your game root path. <br/>

**Step 3: Prepare your mods** <br/>
1. Reuse "1.original_mod" as a template for your mod, from the _modsForGameX/mods_ folder. <br/>
2. Unpack mod files into your new mod template's _files_ folder. <br/>

**Step 4: Install mods** <br/>
Run the shell script _installMods.sh_. <br/>
If everything was done correctly, your mod files will be copied over to your game folder. Enjoy! <br/>

## Frequently Asked Questions
**Q:** I've disabled a mod, then reinstalled, but it appears that the disabled mod is still in the game? <br/>
**A:** That happened because the mod files are already present at your game folder. In the case when you need to remove previous mod files, you need to do a proper cleanup. To do a proper cleanup, you need to execute _cleanMods.sh_. <br/>

**Q:** I've executed _cleanMods.sh_, but mods are still there? <br/>
**A:** You've probably forgot to initialize your game folder as a Git project with _gameCleanInit.sh_. Remove the mod files manually, then copy _gameCleanInit.sh_ over to your game folder and execute it.  <br/>

**Q:** Every time I execute _cleanMods.sh_, all my settings get reset to default/my game's save files disappear. How do I stop that? <br/>
**A:** _cleanMods.sh_ does a hard wipe of all new files, and swaps the changed files to their original versions, even if those files were created by the game itself. To avoid them disappearing, you can create a file named  ".gitignore" at your game root directory, and insert an exclusion rule. <br/>
Here is a list of simple exclusion rules: <br/>
saves/ _Ignore folder "saves"_ <br/>
*.sav _Ignore files with extension ".sav"_ <br/>
**/settings.cfg _Ignore file "settings.cfg", no matter where it is inside of your game folder_ <br/>

Copy over _utils/yamlModder_addGitIgnore.sh_ file to your game root folder and execute. <br/>
Now, every time you execute _cleanMods.sh_, those files will stay. <br/>

**Q:** Everytime I clean mods, my game version downgrades to where it was before I've installed my mods. How do I prevent that? <br/>
**A:** Execute _cleanMods.sh_ so no mods are left, then let the game update itself. Once the game is properly updated, copy over _utils/yamlModder_addGameUpdate.sh_ file to the game root folder and execute. <br/>

Now, every time you execute _cleanMods.sh_, the game will stay up to date. <br/>

## Pro tips
_See what changed:_ <br/>
Go to your game folder and execute command _git status_ <br/>
This command will show you all the files that were added or changed recently. <br/>
With _git add ._ and then _git diff --staged_ you can see all the changes per file. <br/>

## TODO
- Add "enforcedStructure.yaml" to allow the application to resolve uppercase/lowercase issues for mod file and directory names.
- Add "yamlbranchdelete" tool application to allow YamlModder delete path branches, instead of resorting to Git clean as the only option.
- Add "skipNotReplace" flag for each mod, to make it only place its files when they don't exist. Mostly for mod mixing.
- Make YamlModder follow the exact file/folder structure from the YAML's _mod/files/_ folder, instead of just copying the entire thing. Can be useful for when the user would like to tune what exact files he'd like to copy over during mod installation.
