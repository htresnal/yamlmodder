#ifndef MODENTRY_H
#define MODENTRY_H

#include <iostream>
#include <string>
#include <yaml-cpp/yaml.h>


using namespace std;


struct ModEntry {
    bool enabled = false;
    string name = "";
    const YAML::Node *files = nullptr;
};

#endif //MODENTRY_H
