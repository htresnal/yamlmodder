#!/bin/sh


## Specify your game installation folder
INSTALL_DIR="./installFolder/"

## Specify the folder that contains all your mods
MODS_DIR="./mods/"

## Specify the path to the YAML file
YAML_PATH="./mods.yaml"


# Create a YAML based on the modding dir
yamls -fav "${MODS_DIR}" "${YAML_PATH}"

# Install mod
yamlmodder --ignore-case-sort "${YAML_PATH}" "${MODS_DIR}" "${INSTALL_DIR}"
